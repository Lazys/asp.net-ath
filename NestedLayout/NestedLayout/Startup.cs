﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NestedLayout.Startup))]
namespace NestedLayout
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
